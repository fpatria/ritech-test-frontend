import React, { useEffect } from 'react';
import { connect } from "react-redux";
import {getAllPosts, getCategories} from './store/actions'

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import Posts from './Components/Posts'
import Post from './Components/Post'
import CreatePost from './Components/Create'

function App(props) {
  useEffect(() => {
    props.getAllPosts()
    props.getCategories()
  }, []);
  return (
    <Router>
      <Switch>
          <Route exact path="/">
            <Posts />
          </Route>
          <Route exact path="/create">
            <CreatePost />
          </Route>
          <Route exact path="/posts/:id">
            <Post />
          </Route>
        </Switch>
    </Router>
  )
}

const mapStateToProps = (state) => {
  return {
    
  };
};

export default connect(mapStateToProps, {getAllPosts, getCategories})(App);
