import React, { useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Input } from 'reactstrap';
import { connect } from "react-redux";
import {updatePost} from '../../store/actions'

  const EditPostModal = (props) => {
    const [title, setTitle] =  useState(props.post.title)
    const [content, setContent] =  useState(props.post.content)
    const [categories, setCategories] =  useState(props.post.categories.split(','))
    const {
      buttonLabel,
      className
    } = props;
  
    const [modal, setModal] = useState(false);
  
    const toggle = () => setModal(!modal);
    const updatePost = async() => {
      if(title === '') return alert('title cannot be empty')
      if(content === '') return alert('content cannot be empty')
      if(categories.length === 0) return alert('please select at least one category')

      const data = {title, content, categories}

      await props.updatePost(props.post.id, data)
      toggle()
    }
    const titleChanged = (event) => setTitle(event.target.value)
    const contentChanged = (event) => setContent(event.target.value)
    
    const catChanged = event => {
      let opts = [], opt;
      for (let i = 0, len = event.target.options.length; i < len; i++) {
          opt = event.target.options[i];
          if (opt.selected)opts.push(opt.value);
      }
      setCategories(opts)
    }

    return (
      <div>
        <Button color="success" onClick={toggle}>{buttonLabel}</Button>
        <Modal isOpen={modal} toggle={toggle} className={className}>
          <ModalHeader toggle={toggle}>Edit post</ModalHeader>
          <ModalBody>
            <Input  placeholder="Post title" value={title} onChange={titleChanged} />
            <Input  placeholder="Post content" value={content} onChange={contentChanged} />
            <Input value={categories} onChange={catChanged} type="select" name="selectMulti" id="exampleSelectMulti" multiple>
              {
                props.categories.map(cat => (
                  <option value={cat.id}>{cat.name}</option>
                ))
              }

            </Input>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={updatePost}>Save</Button>{' '}
            <Button color="secondary" onClick={toggle}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }


  const mapStateToProps = (state) => {
    return {
      categories: state.categories.categories,
      post: state.posts.post
    };
  };
  
  export default connect(mapStateToProps, {updatePost})(EditPostModal);