import React, { useState, useEffect } from 'react';
import { withRouter, Link } from "react-router-dom";
import {connect} from 'react-redux'
import {getOnePost} from '../../store/actions'
import {Container, Row, Col, Button} from 'reactstrap'
import {deletePost} from '../../store/actions'
import Edit from './edit'
import styles from '../Posts/styles.module.css'

function Post(props) {
    const [post, setPost] = useState(props.post);
    useEffect(() => {
      if(props?.match?.params?.id){
        props.getOnePost(props.match.params.id)
      }
    }, [])

    useEffect(() => {
      setPost(props.post)
    }, [props])
    
   const categoriesToString = (cat) => props.categories.filter(i => cat.includes(i.id)).map(i => i.name).join(' | ')

   const deletePost = async () => {
     if(window.confirm('Are you sure you want to delete post?')){
        await props.deletePost(props.match.params.id)
        props.history.push('/')
     }
   }
    if(!post){
      return 'loaing';
    }
    return (
      <Container>
        <Row>
            <Col style={{margin: '20px 0px'}}>
                <Link to="/">
                  HOME
                </Link>
            </Col>
        </Row>
          <Row>
            <Col xs={12}>
              <p className={styles.post_title}>{post.title}</p>
            </Col>
            <Col xs={12}>
              <p className={styles.post_content}>{post.content}</p>
            </Col>
            <Col xs={12}>
              <p className={styles.post_categories}>{categoriesToString(post.categories)}</p>
            </Col>
            <Col md={3} xs={6}>
                <Edit buttonLabel="Edit" />
            </Col>
            <Col md={3} xs={6}>
                <Button onClick={deletePost} color="danger">Delete</Button> 
            </Col>
          </Row>
      </Container>
    );
  }

  const mapStateToProps = (state) => {
    return {
      post: state.posts.post,
      categories: state.categories.categories
    };
  };
  
  export default connect(mapStateToProps, {getOnePost, deletePost})(withRouter(Post));
  