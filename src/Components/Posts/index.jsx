import React, { useState, useEffect } from 'react';
import { connect } from "react-redux";
import {getAllPosts, getCategories} from '../../store/actions/'
import Post from './post'
import Create from '../Create'
import { Container, Col, Row, Button } from 'reactstrap';

function Posts(props) {
    const [posts, setPosts] = useState([])
    const [categories, setCategories] = useState([])

    useEffect(() => {
      setCategories(formCategories(props.categories))
      setPosts(formPosts(props.posts))
    }, [props])

    useEffect(() => {
      setCategories(formCategories(props.categories))
      setPosts(formPosts(props.posts))
    }, [])
    
    const formPosts = (posts) => {
      return posts.map(item => {
        if(typeof item.categories == 'string')
           item.categories = item.categories.split(',')     
        return item
      })
    }

    const formCategories = (cat) => {
      var c = {}
      cat.forEach(item => {
        c[item.id] = item.name
      })
      return c
    }

    return (
      <Container>
        <div style={{margin: '20px 0px'}}>
          <Create buttonLabel="Create Post" />
        </div>
        {
          posts.map(post => <Post key={`post_${post.id}`} post={post} categories = {categories} />)
        }
      </Container>
    );
  }





  const mapStateToProps = (state) => {
    return {
      posts: state.posts.posts,
      categories: state.categories.categories
    };
  };
  
  export default connect(mapStateToProps, {getAllPosts, getCategories})(Posts);
  