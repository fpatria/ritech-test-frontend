import React, { useState, useEffect } from 'react';
import { Col, Row } from 'reactstrap';
import styles from './styles.module.css'
import {withRouter, Link} from 'react-router-dom'
function Post(props){
    return (
        <Row className={styles.post_wrapper}>
            <Col xs={12} className={styles.post_title} >
                <Link to={`posts/${props.post.id}`}>
                    {props.post.title}
                </Link>
            </Col>
            <Col xs={12} className={styles.post_content}>{props.post.content}</Col>
            <Col xs={12} className={styles.post_categories}>{props.post.categories.map(i => props.categories[i]).join(' | ')}</Col>
        </Row>
    )
}


export default withRouter(Post)