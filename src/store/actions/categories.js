import axios from 'axios'
import * as types from './types'

const API_URL = process.env.REACT_APP_API;

const getCategories = () => (dispatch) => {
    axios.get(`${API_URL}/categories`)
    .then(response => {
        dispatch({
            type: types.CATEGORIES,
            payload: response.data
        })
    })
}

export {
    getCategories,
}