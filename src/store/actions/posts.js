import axios from 'axios'
import * as types from './types'

const API_URL = process.env.REACT_APP_API;

const getAllPosts = () => (dispatch) => {
    axios.get(`${API_URL}/posts`)
    .then(response => {
        dispatch({
            type: types.POSTS,
            payload: response.data
        })
    })
}

const getOnePost = (post_id) => (dispatch) => {
    dispatch({
        type: types.SINGLE_POST,
        payload: null
    })

    axios.get(`${API_URL}/posts/${post_id}`)
    .then(response => {
        dispatch({
            type: types.SINGLE_POST,
            payload: response.data.post
        })
    })
}

const createPost = (data) => (dispatch) => {
    axios.post(`${API_URL}/posts`, data)
    .then(response => {
        data.id = response.data.post_id
        dispatch({
            type: types.NEW_POST,
            payload: data
        })
    })
}

const deletePost = (post_id) => (dispatch) => new Promise((resolve, reject) => {
    axios.delete(`${API_URL}/posts/${post_id}`)
    .then(response => {
        dispatch({
            type: types.DELETE_POST,
            payload: post_id
        })
        resolve(response)
    })
})

const updatePost = (post_id, data) => (dispatch) => {
    axios.put(`${API_URL}/posts/${post_id}`, data)
    .then(respose => {
        data.categories = data.categories.join(',')
        dispatch({
            type: types.UPDATE_POST,
            payload: {post_id, data}
        })
    })
}

export {
    getAllPosts,
    getOnePost,
    createPost,
    deletePost,
    updatePost
}