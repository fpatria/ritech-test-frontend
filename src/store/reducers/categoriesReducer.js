import * as types from '../actions/types'

const initState = {
    categories: [],
  };
  
  const categoriesReducer = (state = initState, action) => {
    switch (action.type) {
      case types.CATEGORIES:
        return {
          ...state,
          categories: action.payload.categories,
        };
      default:
        return state;
    }
  }
  
  export default categoriesReducer;