import * as types from '../actions/types'

const initState = {
    posts: [],
    post: null
  };
  
  const postReducer = (state = initState, action) => {
    switch (action.type) {
      case types.POSTS:
        return {
          ...state,
          posts: action.payload.posts,
        };
      case types.NEW_POST:
        return {
          ...state,
          posts: [action.payload, ...state.posts]
        }
      case types.SINGLE_POST:
        return {
          ...state,
          post: action.payload
        }
      case types.DELETE_POST:
        return {
          ...state,
          posts: state.posts.filter(item => item.id != action.payload)
        }
      case types.UPDATE_POST:
        return {
          ...state,
          post: {
            id: action.payload.post_id,
            ...action.payload.data
          },
          posts: state.posts.map(item => {
            if(item.id === action.payload.post_id) item = {...item, ...action.payload.data}
            return item
          })
        }
      default:
        return state;
    }
  }
  
  export default postReducer;