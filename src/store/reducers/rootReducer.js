import { combineReducers } from 'redux';
import postReducer from './postReducer';
import categoriesReduer from './categoriesReducer'

const rootReducer = combineReducers({
  posts: postReducer,
  categories: categoriesReduer
});

export default rootReducer;